import {Framework, Utils} from "floose";
import Schema = Utils.Validation.Schema;
import Server = Framework.Server;
import WritableStream = NodeJS.WritableStream;

export declare interface Command {
    readonly name: string;
    readonly description?: string;
    readonly arguments?: {name: string, description?: string}[];
    validate?: (args: {[key: string]: string}) => Promise<void>;
    execute: (args: {[key: string]: string}, output: WritableStream) => Promise<void>;
}

export declare interface ConsoleServerConfig {
    prompt: string;
}

export declare class ConsoleServer extends Server {
    readonly configurationValidationSchema: Schema;
    readonly running: boolean;
    init(config: ConsoleServerConfig): Promise<void>;
    start(): Promise<void>;
    stop(): Promise<void>;
    addCommand(command: Command): void;
}